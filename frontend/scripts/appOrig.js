function xhrReq(filePath, success, error)
{
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function()
    {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            if (xhr.status === 200) {
                if (success)
                    success(JSON.parse(xhr.responseText));
        } else {
            if (error)
                error(xhr);
            }
        }
    };
    xhr.open("GET", filePath, true);
    xhr.send();
}


//On document ready function
(function() {
    xhrReq("report.json",
        function(res){
          // var obj = JSON.parse(res)
          console.log(res); 
        })
})();
