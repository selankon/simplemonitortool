'use strict';

/**
 * @ngdoc directive
 * @name frontendApp.directive:matchlist
 * @description
 * # matchlist
 */
angular.module('simpleMonitorApp')
  .directive('semaphore', function () {
    return {
      templateUrl: 'scripts/directives/templates/semaforetemplate.html',
      restrict: 'AEC',
      scope: {
        default : '=',
        result : '=',
        text : '='
      },
      link: function (scope, element, attrs) {
        //State that will define the behaviour of the semaphore
        scope.state = {
          "min-width" : "20px",
          "height" : "20px",
          "border" : "1px solid",
          "display" : "inline-block",
          "margin" : "5px"
        }

        if (scope.result == "null" || scope.result === ""){
          scope.state["background-color"] = "white"
        }
        // Check if default value should be an integer to manage the comparaision
        else if (Number.isInteger(Number(scope.default)) &&
                  Number.isInteger(Number(scope.result))){
          var def = Number(scope.default)
          var res = Number(scope.result)

          // If result and default are 0
          if (def == res && def == 0){
            scope.state["background-color"] = "green"
          }
          // If default is not 0 but equals to result (error expected)
          else if (def == res ){
            scope.state["background-color"] = "gray"
          }
          else {
            scope.state["background-color"] = "red"
          }
        }

      }
    };
  });
