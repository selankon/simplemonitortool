"""Abstract class that define test object
All test objects must to have inheritance of this
"""

from abc import ABCMeta, abstractmethod

class TestABC:
    __metaclass__ = ABCMeta

    @abstractmethod
    def __init__(self, params):
        pass

    @abstractmethod
    def run(self):
        """Implement the test logic
        :return 0 if test passed else return other other number
        """
        pass